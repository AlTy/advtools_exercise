# Exercises for Advanced Computational Tools course 2018 at DTU Energy

Exercises which are being worked on should be put in the `devel` folder. Once they are ready for release,
put them in the `stable` folder - and try to maintain a propper folder structure, i.e. something like "stable/day1/mynotebook.ipynb" for instance.

This gitlab repo is just for the exercise developers. If you want an invite to push to the repo, please ask me, or send an email to <alexty@dtu.dk>.

## Running the setup

Run `source ~alexty/setup.sh` to run the setup installation.

## Running exercises

Use the `ascnotebook` to launch the notebook in the exercise folder. This will launch the notebook in `$HOME/`.

If you want to launch the notebook in your working directory instead, use the `devel-ascnotebook` instead. This shouldn't be necessary
for the students to use, but it's there.

When submitting to the queue, use the `qsub.py` script (it is located in PATH, so in jupyter you can do `!qsub.py -t 1 -p 8 myscript.py`
to submit a 1 hour job on 8 cores).

## Updating exercises

Exercises can be updated using the `ascupdate` script - simply run it, and it will copy the missing files from the stable dir into your folder.