{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Nudge Elastic Band exercise\n",
    "\n",
    "In this exercise we will use the Climbing Image Nudged Elastic Band method to identify transition\n",
    "states for Cu adatom diffusion on Cu(110).\n",
    "\n",
    "We will be using EMT in this exercise, but note that when using DFT methods, NEB easily gets too expensive to run locally, so later in the course we will be submitting these types of calculations to the cluster.\n",
    "\n",
    "## Calculate the lattice of bulk Cu with EMT\n",
    "\n",
    "Perform a series of EMT calculations around the experimental Cu lattice constant and save the calculations to a trajectory. See below for inspiration on how to do this.\n",
    "\n",
    "You can use https://en.wikipedia.org/wiki/Periodic_table_%28crystal_structure%29 and https://en.wikipedia.org/wiki/Lattice_constant to find a good starting value for the lattice parameter of Cu in an FCC lattice. Note that EMT will not find the exact lattice parameter which is known from experiments though!\n",
    "\n",
    "View the files in the ASE gui, and use the Tools/Bulk Modulus functionality to fit an equation of state.\n",
    "\n",
    "Questions:\n",
    "* What is the lattice constant and bulk modulus with EMT?\n",
    "* What is the energy per atom of bulk Cu in EMT?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "from ase import Atoms\n",
    "from ase.io.trajectory import Trajectory \n",
    "from ase.calculators.emt import EMT\n",
    "from ase.build import bulk\n",
    "\n",
    "a = 3.6 # student: a = ?  #  trial lattice constant\n",
    "atoms = bulk('Cu', crystalstructure='fcc', a=a, cubic=True)\n",
    "calc = EMT()\n",
    "atoms.set_calculator(calc)\n",
    "\n",
    "traj = Trajectory('bulk-relax.traj', mode='w', atoms=atoms)\n",
    "\n",
    "scales = np.linspace(0.92, 1.08, 15)  # modify the lattice parameter by +/- 8% in 15 steps\n",
    "\n",
    "for i, scale in enumerate(scales):\n",
    "    atoms.set_cell(scale * np.array((a, a, a)), scale_atoms=True)\n",
    "    e = atoms.get_potential_energy()\n",
    "    traj.write()\n",
    "    print('New a: {:.3f}\\tEnergy: {:.3f}'.format(scale*a, e))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.visualize import view\n",
    "from ase.io import read\n",
    "import numpy as np\n",
    "bulk = read('bulk-relax.traj@:')\n",
    "view(bulk)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# teacher\n",
    "# optimal lattice constant\n",
    "'{:.3f}'.format(46.258**(1/3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Set up a Cu(110) surface using the ase.build.surface module\n",
    "\n",
    "In the following, we will set up a Cu(110) surface with a Cu adatom, see the below script for inspiration.\n",
    "\n",
    "Question:\n",
    "* What is the reaction energy of $$\\text{Cu(bulk)} + \\text{Cu(110)} \\rightarrow \\text{Cu@Cu(110)}$$ where Cu@Cu(110) is the (110) surface with an adatom."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "from ase import Atoms\n",
    "from ase.calculators.emt import EMT\n",
    "from ase.optimize import BFGS\n",
    "from ase.build.surface import fcc110, add_adsorbate\n",
    "from ase.constraints import FixAtoms\n",
    "\n",
    "a0 = 3.590 # student: a0 = ? # use calculated lattice constant\n",
    "\n",
    "size = (4, 4, 7)\n",
    "slab = fcc110('Cu', a=a0, size=size, vacuum=7.)\n",
    "cell = slab.get_cell()\n",
    "\n",
    "# optimize the top 4 layers\n",
    "mask = [a.tag > 4 for a in slab]\n",
    "slab.set_constraint(FixAtoms(mask=mask))\n",
    "\n",
    "# add adsorbate where you want it\n",
    "pos = np.array([1, 0])  # Adjust to move around adsorbed Cu atom - offset in (x, y) direction by 1 lattice vector\n",
    "posname = 'a' + ''.join(str(p) for p in pos)  # a00, a10, a01\n",
    "\n",
    "add_adsorbate(slab, 'Cu',\n",
    "              0.7*a0,  # Height above slab\n",
    "              position=pos*a0,  # Position above the Cu slab.\n",
    "              offset=(0.5, 0.5))\n",
    "\n",
    "if 0: # for viewing only. Set to 1 to view, and 0 to ignore\n",
    "    from ase.visualize import view\n",
    "    view(slab)\n",
    "    assert False # Hack to break the execution by raising an error intentionally.\n",
    "\n",
    "# Relax the cell using EMT\n",
    "calc = EMT()\n",
    "slab.set_calculator(calc)\n",
    "\n",
    "dyn = BFGS(slab, trajectory='Cu110_{}.traj'.format(posname))\n",
    "dyn.run(fmax=0.01)\n",
    "e_110 = slab.get_potential_energy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## CI-NEB\n",
    "\n",
    "Use the CI-NEB method to calculate activation energies for Cu adatom diffusion on Cu(110). See the script below for inspiration on how to run the CI-NEB calculation in ASE. The initial and final states are constructed in the above script.\n",
    "\n",
    "You can use the ase-gui to plot minimum energy pathway (MEP) using the command `ase gui CINEB.traj@-10:` and then chose the menu Tools/NEB. Remember that you can run terminal commands in the notebook by starting the line with `!`. Note that the `-10` at the end of the command means \"look at the last 10 images of `CINEB.traj`. The reason we chose 10, is because we chose to have 10 images in total.\n",
    "\n",
    "Questions:\n",
    "* Show a MEP for diffusion in the [0 0 1] direction (hopping over a \"long bridge\")\n",
    "* Show a MEP for diffusion in the [1 -1 0] direction (hopping over a \"short bridge\")\n",
    "* Can you imagine other diffusion steps?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "from ase.io import read, write\n",
    "from ase.calculators.emt import EMT\n",
    "from ase.neb import NEB\n",
    "from ase.optimize import LBFGS as Optimizer\n",
    "\n",
    "initial = read('Cu110_a00.traj@-1') # read structure of initial state\n",
    "final = read('Cu110_a01.traj@-1') # read structure of final state\n",
    "\n",
    "# initialize NEB\n",
    "Nimages = 8 # number of images\n",
    "\n",
    "configs = [initial]\n",
    "for i in range(Nimages):\n",
    "    config = initial.copy()\n",
    "    config.set_calculator(EMT())\n",
    "    configs.append(config)\n",
    "configs.append(final)\n",
    "\n",
    "band = NEB(configs)\n",
    "band.interpolate()\n",
    "\n",
    "# optimize NEB\n",
    "relax = Optimizer(band, trajectory='a01-NEB.traj')\n",
    "relax.run(steps=50, fmax=0.1)\n",
    "\n",
    "if 1: # CINEB\n",
    "    ciband = NEB(configs, climb=True)\n",
    "    cirelax = Optimizer(ciband, trajectory='a01-CINEB.traj')\n",
    "    cirelax.run(steps=200, fmax=0.02) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ase gui a01-CINEB.traj@-10:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Direction: 10\tBarrier: 0.832\n",
      "Direction: 01\tBarrier: 0.289\n"
     ]
    }
   ],
   "source": [
    "# teacher\n",
    "from ase.neb import NEBTools\n",
    "from ase.io import read\n",
    "for direction in ['10', '01']:\n",
    "    atoms = read('a{}-CINEB.traj@-10:'.format(direction))\n",
    "    neb = NEBTools(atoms)\n",
    "    eb = neb.get_barrier()[0]\n",
    "    print('Direction: {}\\tBarrier: {:.3f}'.format(direction, eb))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Vibrational Analysis\n",
    "\n",
    "Calculate vibrational frequencies for the initial state and transition states. See below for inspiration on how to do this.\n",
    "\n",
    "Questions:\n",
    "* How many imaginary modes are there at the initial states and transition states?\n",
    "* Describe the imaginary mode(s) of the transition state?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "from ase import Atoms\n",
    "from ase.calculators.emt import EMT\n",
    "from ase.vibrations import Vibrations\n",
    "\n",
    "from ase.io import read\n",
    "\n",
    "slab = read('Cu110_a00.traj@-1') # read initial or transition state structure\n",
    "slab.set_calculator(EMT())\n",
    "\n",
    "vibindices = [atom.tag < 1 for atom in slab] # vibrate only atoms with tag < 0\n",
    "                                             # (here it means: vibrate only adatom)\n",
    "vibindices = np.array(vibindices).nonzero()[0] # extract indices of atoms to be vibrated\n",
    "\n",
    "vib = Vibrations(slab, \n",
    "                 name='initial-vib', # Can be a good idea to adjust this name, for different systems\n",
    "                 indices=vibindices, nfree=4, delta=0.01) # set up vibrational analysis calculation\n",
    "                 # slab - Atoms object for which we are running vib\n",
    "                 # indices - indices of atoms to be vibrated\n",
    "                 # nfree - method; 4 displacements (see Wikipedia: five-point stencil)\n",
    "                 # delta - displacement\n",
    "                 # See documentation for Vibrations: https://wiki.fysik.dtu.dk/ase/ase/vibrations/vibrations.html\n",
    "vib.run() # run vibrational analysis calculation\n",
    "vib.summary() # show vibrational modes' wavenumbers\n",
    "\n",
    "for i in range(3):\n",
    "    vib.write_mode(i) # write modes\n",
    "\n",
    "#    You can visualize normal modes with\n",
    "#         ase gui vib.x.traj\n",
    "#    in the console, where x is the mode number (0, 1 or 2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ase gui initial-vib.0.traj"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Rate Analysis\n",
    "Use transition state theory to estimate the rate of atom hopping in the [1 -1 0] and [0 0 1] directions as function of temperature.\n",
    "\n",
    "Plot this estimate as function of temperature."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAZIAAAEKCAYAAAA4t9PUAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDMuMC4wLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvqOYd8AAAIABJREFUeJzt3Xl8VOXZ//HPlX0PkBC2AIkQCCh7gCJg2VRUBJ+KIlrBokWt2GqfVktbq7a2+lh/bd1aBaG44L60iohaEVmrhEXZIexhywIhezLJ3L8/zgRCyMpkcmYm1/v1Oq+Zc8+ZmetAyJdz7nPuW4wxKKWUUhcqwO4ClFJK+TYNEqWUUm7RIFFKKeUWDRKllFJu0SBRSinlFg0SpZRSbtEgUUop5RYNEqWUUm7RIFFKKeWWILsLaAnx8fEmKSnJ7jKUUsqnbNiwIccY076h7VpFkCQlJZGenm53GUop5VNE5GBjtvP6U1sicpGILBCRd2u0R4pIuohMsqs2pZRSNgWJiCwUkSwR2VqjfaKI7BKRDBH5FYAxZp8x5vZaPuZB4O2WqFcppVTd7DoiWQRMrN4gIoHA88BVQF9guoj0re3NInI5sB3I8myZSimlGmJLH4kxZqWIJNVoHgZkGGP2AYjIm8AUrMCoaQwQiRU4JSKy1BjjbEoNDoeDzMxMSktLm1i9ak5hYWEkJiYSHBxsdylKqQvkTZ3tXYDD1dYzgeEiEgf8ERgkInONMY8bY34DICK3ATm1hYiIzAZmA3Tr1u28L8vMzCQ6OpqkpCREpNl3RjXMGENubi6ZmZkkJyfbXY5S6gJ5U5DUyhiTC9xVx2uL6nnfPGAeQFpa2nmzd5WWlmqI2ExEiIuLIzs72+5SlFJu8Karto4AXautJ7raPEZDxH76d6CU7/OmI5L1QIqIJGMFyE3AzfaWpJRSPsIYKDkFBceh4BgUnrAe23SHflM9+tW2BImIvIHVYR4vIpnAw8aYBSIyB/gUCAQWGmO22VFfS/njH//I66+/TmBgIAEBAbz44osMHz78zA2U8fHxF/S5mzdv5ujRo1x99dXnvbZixQqmTJlCcnIyTqeThIQEXn/9dRISEs7bNj09nVdeeYVnnnnmvNfcrVEp1QRlBZB/zAqGM4srMPJdzwuPQ2X5+e/tM9k/g8QYM72O9qXA0hYuxxbr1q1jyZIlbNy4kdDQUHJycigvr+WHoIkqKirYvHkz6enptQYJwOjRo1myZAkAc+fO5fnnn+fRRx8973PS0tJIS0tzuyalVB2MgeKTkH8E8o9We3Q9rwqK8oLz3xsaC9EdraX7COsxquPZtqgO1mNIpMd3w5tObbUqx44dIz4+ntDQUIDz/mf/7LPP8tFHH+FwOHjnnXdITU3l5MmTzJo1i3379hEREcG8efPo378/jzzyCHv37mXfvn1069aNNWvWUFJSwurVq5k7dy7Tpk2rtQZjDAUFBfTs2RPgvM+58847eeqpp1iyZAm5ublMnz6dI0eOMGLECIw5e/3CH/7wB1577TXat29P165dGTJkCL/4xS/Yu3cv99xzD9nZ2URERDB//nxSU1M99CeqlBcqzYfTmdaSnwmnj1gBcTrzbGhU1LgFQQKtAIjpDO1Tocc4iO4EMV3Otkd1gNAoe/apFhokwKMfbWP70fxm/cy+nWN4+NqL63z9iiuu4Pe//z29evViwoQJTJs2je9///tnXo+Pj2fjxo38/e9/56mnnuKll17i4YcfZtCgQfzrX/9i+fLlzJgxg82bNwOwfft2Vq9eTXh4OIsWLSI9PZ3nnnuu1u9etWoVAwcOJDc3l8jISP70pz+dea3656xYseJM+6OPPsqoUaP43e9+x8cff8yCBQsAWL9+Pe+99x7ffvstDoeDwYMHM2TIEABmz57NCy+8QEpKCl9//TU/+clPWL58+QX/mSrlVYyBomzIO3R2qQqN04etpfT0ue+RgLOh0Gkg9L4aYhOtcIjpYi1RCRAQaM8+XSANEptERUWxYcMGVq1axZdffsm0adN44oknuO222wD4wQ9+AMCQIUN4//33AVi9ejXvvfceAOPGjSM3N5f8fCsAJ0+eTHh4eKO+u/qprf/7v//jgQce4IUXXqj3c1auXHmmjmuuuYa2bdsCsGbNGqZMmUJYWBhhYWFce+21ABQWFrJ27VpuuOGGM59RVlbW+D8gpexmDBTlQN5BOHXAFRYHzw2NmkcTYbEQ29Vauo2wQiI20dXWxTr1FOh/v3b9b48uQH1HDp4UGBjImDFjGDNmDP369ePll18+EyRVp7wCAwOpqKho8LMiIy/sPOjkyZO5/vrr3f6cmpxOJ23atDlzxKSUV3KUng2KquXkfldwHARH8bnbR8RBm27Q4WLofRXEdrPW27jCIyym5ffBC2iQ2GTXrl0EBASQkpICWFdade/evd73jB49msWLF/PQQw+xYsUK4uPjiYk5/wc3OjqagoJaOudqsXr1anr06NHgdpdddhmvv/46v/3tb/nkk084deoUACNHjuTOO+9k7ty5VFRUsGTJEmbPnk1MTAzJycm888473HDDDRhj+O677xgwYECj6lKq2ZTmw8l91nJqv+v5Aet5fo1b1YIjoW0StLsIeoy1Lp1t2916bNMVQqPt2AOvp0Fik8LCQu69917y8vIICgqiZ8+ezJs3r973PPLII8yaNYv+/fsTERHByy+/XOt2Y8eO5YknnmDgwIG1drZX9ZEYY4iNjeWll15qsN6HH36Y6dOnc/HFF3PppZeeGXZm6NChTJ48mf79+9OhQwf69etHbGwsAIsXL+buu+/msccew+FwcNNNN2mQKM8oL7YCIncP5O51Pd8LJ/da/RjVRXWAtsmQ/H1ol2wFR1vXY2Q86E2yTSbVr77xV2lpaabmxFY7duygT58+NlXkXwoLC4mKiqK4uJjLLruMefPmMXjw4Ea/X/8uVKM4ndYRRM5uyNnjCo0MyMmwroiqLqojxPWwjiyqHtv1sMLCi6528nYissEY0+A9AHpEotw2e/Zstm/fTmlpKTNnzmxSiCh1nopy64gie6crNHZD9i4rNKr3WYTGQFxPSBppPcb1dIVGj1YVFpVOQ4mjkpLySkod1lK1XuKopG1ECAO6tvFoDRokym2vv/663SUoX1RRboVD9g7I2mkFR/Yu63SUs9oFJm26QXwvSBoF8SnW87gU6zJZHzgN5XQaSisqKSqzfrkXOyooLq+kuKyS4vIKShyV1np5JSXl1mtVQVD9edV2pdXWSxyVlFfUP4PGFX07MG+GZ28s1iBRSnmW02ldAXViG2TtgKzt1mPunrOBIQHW6af2qdBnkvXYvrcVGCERLVaqMYZSh5OCMgdFZZUUlVVQVGb9ci8sq6C4vILCskqKyyooKq90rVdQXFZJkSsEisqscChyBUVxeWWTaggMECKCAwkPcS3BgUS4nreNCCY8JOjM62HB1uvhIQGEhwRZz13rYcHW6/GRoR760zpLg0Qp1XxK863AOLHV9bjNCo7ywrPbtOkOCX2ty2cT+kJCqhUYwWEX/LWOSieFpRUUlFZQUOagsNT6BV9YVkF+aYVr3QqHglIrHAqrLUXVHp2N7DYOCQogMiSQiJAgIkMDCQ8JIio0kHaREURUtYdYIRARGmSFQXAgkaFBhIcEEhFsbRNetY0rLEICA3xuVGwNEqVU0xljjQN1fAsc/871uMXq26gSFgsdLoGBN1uPHS62jjRq9F8YYygqryQ/r4T8Ugf5JRXklzhczx3kl1ZQUOqgoLSC/DOPZ9sKSh2UOhqeIDVAICo0iOiwYCJDA4kKDSImPJjObcKIDAkiMjSIqNCqR+sXfmRokOu1wDOvRYYEEREaSHCgN83CYS8NEqVU/Yyx7uI+thmOboZj31rPq11Wa9om40y4hKLeN5AXk0p2ZArZEsfp0gryih3kZTs4fcjB6ZLd5Jc4OF3iOPtYWkFlA4cBYcEBxIQFEx1m/fKPDQ8msW040aFBRIdZ4RBV7Xl0mBUKUWFBRLsew4MDfe5/+r5Cg8RGUVFRFBYWNrxhM7jtttv46quviI2NpbS0lOnTp/Pwww/Xuu3vfvc7LrvsMiZMmHBO+4oVK84M4qj8lzP/OMUH1uM4tJ6AY5sIz9lCSJl1A6qTQE6EdWdf0GB2RV/EFmd3Npd14Uh2MOXHqh8VHHItlqAAITY8mNiIYGLCgmkbEUJSXCSx4cHEhAcRExbsem69XtVWFQwhQfq/f2+mQdKK/PnPf2bq1KmUlpbSt29fZsyYcd5c6ZWVlfz+97+3qULlCZVOw6nicnILy8ktLCOnqJyThWWcLConP/80Mae20iF/C91KtpNSsYeO5BIFVBpht0lki7M/W0wyW53JZAQkEx4cSdvQENpEWIEwPMIKiDbhVlub8LPrsa71iBA9GvBnGiRe5sCBA8yaNYucnBzat2/PP//5T7p168Ztt91GTEwM6enpHD9+nCeffJKpU6fidDqZM2cOy5cvp2vXrgQHBzNr1iymTq17IpvSUmuguapxtZKSkpg2bRqff/45DzzwAMuWLWPSpElMnTqVZcuWcd999xEREcGoUaPOfEZ2djY333wzR48eZcSIEXz++eds2LCB+Ph4XnvtNZ555hnKy8sZPnw4f//73wkM9K3RTL1dpdOQW1RGVn4ZOYVlZBeUkVNY7no8u+QWlnOyuBzrvmNDshxnsOxhUMAergzIoHfAYYKwjiSygjtzrM1Adra5hKL4gZiO/YiJaUOfiBBGRATTLjJEA0HVSoME4JNfWR2FzaljP7jqiSa/7d5772XmzJnMnDmThQsX8tOf/pR//etfgDWHyerVq9m5cyeTJ09m6tSpvP/++xw4cIDt27eTlZVFnz59mDVrVq2f/ctf/pLHHnuMjIwMfvrTn54zK2JcXBwbN24EYNmyZYAVOD/+8Y9Zvnw5PXv2PGeolUcffZRx48Yxd+5cli1bdmZY+R07dvDWW2+xZs0agoOD+clPfsLixYuZMWNGk/8sWqOKSic5heWcyC+1loIyTpwuJauglKwCKziyC8vILSyr9eqiiJBA2keHEhcZQs+2wfygfSZ9HdtJKt5Kh9PfElruOkUVEo0kpiGJ0yBxKHQZQkJkHOfPk6lUw3wySESkL/AIkAt8YYx5196Kms+6devODNd+66238sADD5x57brrriMgIIC+ffty4sQJwBp08YYbbiAgIICOHTsyduzYOj+76tRWYWEh48ePZ+3atVx66aUAtU5+tXPnTpKTk88MLPnDH/7wzHhgq1ev5oMPPgBg4sSJZ4aV/+KLL9iwYQNDhw4FoKSkpNZpfFujUkclx0+XcvR0CcdPl3I8v9R6rPY8p5aACAwQ4qNCSIgOo2NsGP0TY0mIDqV9tSU+KpT44DIiT2yEg1/BwbVweNPZqVfb9YC+V0PX4dB1OAHxvSBA+x1U8/CaIBGRhcAkIMsYc0m19onA01jzuL9kjHkCuAp41hizSkQ+BNwLkgs4crBD1dDyAO6MkRYVFcWYMWNYvXr1mSBpruHjjTHMnDmTxx9/vFk+z1cYYzhV7ODIqRKO5BWTeaqEI3klHM0r4djpUo7mlZBTeP5UyjFhQXSKDadjbBh9OsbQITaMDjGhdHCFRkJMKHGRoQQG1HI6qeSUFRg718KB1dZluMYJAUHQeRAMvxO6fs8Kj6j2LfCnoForrwkSYBHwHPBKVYOIBALPA5cDmcB6V3C8CjwsIpOBuJYv1XMuvfRS3nzzTW699VYWL17M6NGj691+5MiRvPzyy8ycOZPs7GxWrFjBzTffXO97Kioq+Prrr7n33nvr3S41NZUDBw6wd+9eevTowRtvvHHO97799ts8+OCDfPbZZ2eGlR8/fjxTpkzh/vvvJyEhgZMnT1JQUNDgEPm+4HSJg8Mni8k8VczhkyUcPlXsWi8h81QJJY5z72CODAmkc5twOrUJ5+LOMXSKDadTbJjVFmsFRURIE/4JlhXCoXWw/yvYvxKOfQcYCAy1Tk9d9kvofqn1vAXm6VaqitcEiTFmpYgk1WgeBmQYY/YBiMibwBRjzOPAPa6geb+2zxOR2cBs4MyQ596muLiYxMTEM+s///nPefbZZ/nRj37En//85zOd7fW5/vrr+eKLL+jbty9du3Zl8ODBZ4Zxr6mqj6S8vJzx48efmYWxLmFhYcybN49rrrmGiIgIRo8efWaek6ph5V999VVGjBhBx44diY6OJj4+nscee4wrrrgCp9NJcHAwzz//vE8EiTGG7MIyDuYWcyCniEMnizmQW8yh3CIOniwmr9hxzvbRYUF0bRvBRe0jGZ3SnsS24XRpG06XNuEktg0nNjzYvY7pSgdkpsO+L2HfCjiywRpSJDAEEofBmLmQPBq6DIEgzw+DoVRdvGoYeVeQLKk6tSUiU4GJxpg7XOu3AsOBp4BfA5HAP4wxq+v7XH8fRr5qGPfc3FyGDRvGmjVr6Nixo0e/s6ysjMDAQIKCgli3bh133333Bc+G2NJ/F4VlFezNKmR/TtF5S2HZ2cECAwS6tA2ne7tIusdF0K1dBF3buR7bRhAbEdy8hRlj3Rm+dzns/dI66igvsMah6jzImj8j+TLrVFULjj+lWi+/HkbeGHMA19GGgkmTJpGXl0d5eTkPPfSQx0ME4NChQ9x44404nU5CQkKYP3++x7+zqXIKy9h9ooC9WYXszS4iI6uQjKxCjuefnWdbBBLbhpMcH8WQ7m1JiosgKT6SpLhIurQN9/wwGOXFVv/Gns+sJe+g1d6mG/SbCj3GWUcd4W09W4dSbvD2IDkCdK22nuhqU9WsWLGixb8zJSWFTZs2tfj31qag1MHuEwXsOl7oeixg94kCcovOdm5HhgTSIyGKS3vE0SMhih7to+jRPpKu7SIIC27he1xO7oc9n1vBcWAVVJRCcIR1xHHpvVZ4tLvIJ4ZIVwq8P0jWAykikowVIDcB9fckN4ExRm+usllTTq0aYzh8soTtx/LZUbUcz+fwyZIz20SGBJLSIZoJfTrQq2M0vTpE0TMhio4xYfb9XTudcHQT7PoYdi615t8A65LcIT+ClMuh+0i3Rr9Vyk5eEyQi8gYwBogXkUzgYWPMAhGZA3yKdfnvQmPMtub4vrCwMHJzc4mLi9MwsYkxhtzcXMLCzv8FaozhYG4xW46ctpbM02w9epqCUqsPQwSS4yLp36UNNw3tRmrHaHp1iKZLm3ACartUtqVVlMG+r6zw2LUMCo+DBFpXVQ1+HHpdac3mp5Qf8KrOdk+prbPd4XCQmZl5ZrgQZY+wsDASExPJK3Wy+XAemw+fYtOhPLYcORsaIYEBpHaK5pIusVzcOYa+nWLo3TG6aZfOtgRHCWR8Adv/DbuXQVk+hERBz/HQ+xrryCOind1VKtVoft3Z3hyCg4PPG7BQtYyKSifbj+WTfuAUmw7nsenQfjJPWaenggKE1E7RXDugM/26xNKvSyy9OkR77+iv5cVWX8f2f8PuT8FRZHWM951iLcmX6aW5yu+12iBRLaekvJJNh0+xfv8p1h84ycZDp85MP9o5NoxB3doyc0QSg7q14ZIusS3f+d1UlQ7rEt0t71h9Ho4iiGwP/W+0wiNpFAQ286XBSnkxDRLV7MoqKtl4MI91e3NYuzeXzYfzqHAaRCC1YwxThyQyNKkdaUlt6RQbbne5jeN0wqG1sOVd2P4va3iS8LbQ/wa45HqrszzAywNQKQ/RIFFuczoN247msyojm7UZuaw/cJKyCicBAv0S23DH6IsYntyOwd3bEhvuY/9Tz90Lm1+Hb9+E/EzrMt3Ua+AS1z0eQSF2V6iU7TRI1AXJKy5n5Z4cVuzKYuXu7DMDEqZ2jObm4d0Y2SOeYRe1IybMx4IDoPQ0bPvACpDDX1t3lvcYB5c/Cr2v0nGslKpBg0Q1ijGGPVmFfLbtOMt3ZrH5cB5OA20igrkspT1jerdndEp72kf7aMeyMXBwDWxYBDs+sm4SjO8NEx6F/tMgppPdFSrltTRIVJ2cTsPmzDw+3Xacz7adYH9OEQD9E2OZMy6FMb3bMyCxTe1DnPuK4pPWkceGRZC7B0JjYeAtMOgW6DxY7y5XqhE0SNQ5nE5D+sFTfPTtUT7ddpysgjKCAoQRPeKYNSqZK/p2oEOMj9+BbYw1HHv6Quuy3cpyazTd6/4Bfa/TARGVaiINEoUxhh3HCvj3t0f4aPNRjp4uJSw4gLG9E7jy4o6MTU3wvU7y2jhKrEt2v34RTmy1jj6G3GYtHS62uzqlfJYGSSt2NK+E9zdm8u/NR9mTVUhQgHBZr/Y8eFUqE/p0IDLUT348TmfC+gXW6auSk9DhErj2Geh3gx59KNUM/OQ3hWosR6WT5TuzePObQ3y1OxungWFJ7Xjsuku4ul8n2kX60eWsmemw9lmr8xxjXbY7/C7rng/t+1Cq2WiQtBIHc4t4a/1h3tmQSXZBGR1iQvnJmJ5MG9qVru386H/lxkDGf2D13+DgagiLhRH3wNA7oK33z9KolC/SIPFjxhhWZ+Tw0qr9fLU7mwCBcakJ3DS0G2N6tyfI05M2taTKCtj2Pqx52ur/iOkCV/4JBs+E0Ci7q1PKr2mQ+KFSRyUfbj7KgtX72XWigPioUO6f0ItpQ7vSMdbHr7iqqaIMNr0Kq5+G04egfap19dUlU/Wuc6VaiAaJH8ktLOPV/x7ktf8eJKewnNSO0Tx1wwCuHdCJ0CA/Gweqogw2vQar/mINXZI4DK5+ElKuhAA/OtJSygdokPiBk0XlvLhyL6+sPUiJo5JxqQncMSqZET38cNKuinLY/Bqs/H9nA2TKs3DRWO1AV8omGiQ+7FRROfNX7ePltQcodlQyeUBn7h3Xk54J0XaX1vwqK2DzYlj5Zzh9GBKHwuRnrDGwNECUspXPBomIRAJfAY8YY5bYXU9Lyisu56VV+1m09gBF5RVc068TPxufQkoHPwwQY2DXJ/CfRyBnF3RJg2v/Bj3Ga4Ao5SW8JkhEZCEwCcgyxlxSrX0i8DTWnO0vGWOecL30IPB2ixdqI0elk9f+e5C/fr6b/FJXgExIoZc/BghY94F89pA1D0hcT5i22LoXRANEKa/iNUECLAKeA16pahCRQOB54HIgE1gvIh8CXYDtgJ9dglS3lbuz+f2S7WRkFTKqZzy/uaYPfTrF2F2WZ+TuhS8etcbBikyAa/4Cg2forINKeSmvCRJjzEoRSarRPAzIMMbsAxCRN4EpQBQQCfQFSkRkqTHGWf2NIjIbmA3QrVs3zxbvQQdyinjs4x38Z8cJusdFMH9GGhP6JPhfJzpAWYHVB7Lu7xAYAmPmwog5eh+IUl7Oa4KkDl2Aw9XWM4Hhxpg5ACJyG5BTM0QAjDHzgHkAaWlpxvOlNq+S8kqe/mIPC1bvIyQwgAcnpjJrVJL/XcYLVj/Ilnfh84eg4BgM+iGM+x1Ed7C7MqVUI3h7kNTLGLPI7ho8YcPBk/zine/Yn1PE9YMTeXBibxJ8fej2uhzfCp88YE0q1XkQTHsNEtPsrkop1QTeHiRHgK7V1hNdbX6p1FHJXz7fzfxV++gcG87rdwzn0p7xdpflGaX58OUf4Zv51nhY1z4Ng2bozYRK+SBvD5L1QIqIJGMFyE3AzfaW5BmbD+fxv29vZm92EdOHdePXV6cS7YvznTfG7k9hyf3Waay0WTD2NxDRzu6qlFIXyGuCRETeAMYA8SKSCTxsjFkgInOAT7Eu/11ojNlmY5nNrrzCyd/+s5sXvtpLh5gwXpk1jMt6tbe7LM8oyoFlv7Iml0roCze+ColD7K5KKeUmrwkSY8z0OtqXAktbuJwWcSK/lLte28CmQ3ncmJbIbyf1JcYfj0KMga3vWX0hpfnW1Vijfq6DKirlJ7wmSFqbb/af5CeLN1JcXsFzNw9iUv/OdpfkGfnHYMl9sHsZdBkCk5+DDn3trkop1Yw0SFqYMYaX1x7gsY930LVdBK//eLj/3pm+Ywl8OAccpXDFH+F7d0OAH16+rFQrp0HSgkrKK/nNB1t4f9MRJvRJ4P/dOJDYcD88lVVeBJ/+2pojvdMAuH4BxKfYXZVSykM0SFpI5qliZr+ygR3H8/n55b2YM7YnAQF+eHf60c3w3h2QmwEj77OuyNK+EKX8mgZJC9hzooAfLvia4vJKFs4cytjUBLtLan5OJ6x9BpY/BpHtYeaHkHyZ3VUppVqABomHbT1ymhkLvyFAhHfuGkFqRz8caLEoF967HfZ9CX0mWzcX6n0hSrUaGiQetP7ASWb9cz0x4cEsvmM4SfGRdpfU/I5ugrdmQOEJK0AGz9Rh3pVqZTRIPOSr3dnc+Wo6nduE89rtw+ncJtzukprfxlfh4/+FqASYtQy6DLa7IqWUDTRIPOCTLcf46ZubSEmI5pXbhxEfFWp3Sc2rosy6uXDDIrhoDFy/ECLjbC5KKWUXDZJm9v7GTH7xzrcM6taWhbcN9b/Le09nwtsz4MgGGHU/jHtI7w1RqpXTIGlGX+7K4pfvfseIHnHMn5FGRIif/fEe2QivTwNHsTVOVt/JdleklPICfvabzj5bj5zmnsUbSe0YzYu3+mGI7FxqXZkVGQ8zP4KEVLsrUkp5CZ38oRkcySvhR4vW0zYihIW3DSUq1M9C5Jv58NYt0L433PGFhohS6hx+9huv5Z0ucfCjf35DqaOSxXcMp4M/zWTodFrT3657DnpdBVMXQIgfXsKslHKLBokbyiuc3PXqBvbnFPHyrGH+NfiiowQ+uBO2/xuGzYaJT2inulKqVhokF8gYw4Pvfce6fbn8ddoALu3hR1PilpyyOtUPf2ON2jviHr3JUClVJ58MEhG5CPgNEGuMmWpHDX/5fDcfbDrCL67oxf8MSrSjBM8oPgmvTIHsnXDDIrj4OrsrUkp5uRbvbBeRhSKSJSJba7RPFJFdIpIhIr+q7zOMMfuMMbd7ttK6rdiVxbPLM5iW1pV7xva0q4zmV5gFiyZB9i646Q0NEaVUo9hxRLIIeA54papBRAKB54HLgUxgvYh8iDVP++M13j/LGJPVMqWeL7ewjF+++x29O0Tz6JSLEX855ZN/DF6ZDHmH4Za3rTvWlVKqEVo8SIwxK0UkqUbzMCB+ThxXAAAWgklEQVTDGLMPQETeBKYYYx4HJrVshXUzxjD3/S2cLnbwyqxhhAX7Sefz6Ux4+VrriOSH70HSSLsrUkr5EG+5j6QLcLjaeqarrVYiEiciLwCDRGRuHdvMFpF0EUnPzs5uliLfWn+Yz7af4IGJvenTyU+Ggz91AP55FRTlwK0faIgopZqsUUckIhIADAA6AyXAVjtPLxljcoG7GthmHjAPIC0tzbj7nftzinj0o+2M7BnHrJHJ7n6cdzi53+oTKS+EGf/W0XuVUhek3iARkR7Ag8AEYA+QDYQBvUSkGHgReNkY43SzjiNA12rria42r+CodHLfW5sJCQrgqRsG+McUuQUn4NX/AUcR3LYEOvazuyKllI9q6IjkMeAfwJ3GmHP+Vy8iCcDNwK3Ay27WsR5IEZFkrAC5yfXZXuHZ5Rl8eziP528eTKdYP5hXpPQ0vHa9NRnVjA81RJRSbqk3SIwx0+t5LQv4W1O/UETeAMYA8SKSCTxsjFkgInOAT7Gu1FpojNnW1M/2hA0HT/Hc8j38YHAXrunfye5y3OcohTemQ/YOuPkt6DrU7oqUUj6usX0kNwDLjDEFIvIQMAh4zBizsalfWFc4GWOWAkub+nmeVFhWwf1vbaZzm3AenXyx3eW4r7IC3p0FB9fC9S9Bzwl2V6SU8gONvWrrIVeIjALGAwuwTnn5tQM5RZQ6KvnbtIFEh/n4BFXGwJKfwa6P4aonoZ8tAwIopfxQY4Ok0vV4DTDPGPMxEOKZkrzHJV1iWfnAWNKS2tldivv+8zBseg2+/yAMn213NUopP9LYIDkiIi8C04ClIhLahPf6NL+46fC/L8CapyHtdhhT6203Sil1wRobBjdidYRfaYzJA9oBv/RYVar57P0SPv019L4Grv6zjuKrlGp2Dd1HsgFYDXwCLDXGlAIYY44BxzxfnnLLyX3wzm0Q3wt+8KLOJ6KU8oiGjkiGAx9gXa77lYgsFZGfiUgvj1em3FNWCG/eYj2f/jqE+tGkW0opr9LQfSQVwArXgoh0BiYCj4lIT+C/xpifeLhG1VROpzW7YfZO+OH70O4iuytSSvmxJo3+a4w5CiwEFrrG3xrhkaqUe1b+GXYugSv/BD3G2l2NUsrPuXPl1QvGmDXNVolqHjuWwIo/wYDp8D09WFRKeV5Dne113UAhwNXNX45yS9YO65RW58Ew6W96hZZSqkU0dGorGziIFRxVjGs9wVNFqQtQXgRv3QohkXDTYggOs7sipVQr0VCQ7APGG2MO1XxBRA7Xsr2yy6e/gdwMa16RmM52V6OUakUa6iP5G9C2jteebOZa1IXa+TFs+CeM/Clc9H27q1FKtTINXf77fD2vPdv85agmKzgO/54DnQbA2N/aXY1SqhWq94jENdpvfa/HiMglzVuSajSnEz64Cxwl8IOXIMjvx9FUSnmhhvpIrheRJ4FlwAbOTrXbExgLdAf+16MVqrp9/Q/Y9yVM+iu018EGlFL2aOjU1v2uS4CvB24AOgElwA7gRWPMas+XqGp1fAv85xFrMMYhP7K7GqVUK9bgne3GmJPAfNfiFVx31f8BiAHSjTHuzhnvWxwl8N4dEN4WJj+r94sopWzV4nOKiMhCEckSka012ieKyC4RyRCRXzXwMVOARMABZHqqVq/12UPWOFrX/QMi4+yuRinVyjVprK1msgh4DnilqkFEAoHngcuxgmG9iHwIBAKP13j/LKA3sNYY86KIvAt80QJ1e4eD62D9fBh+N/Qcb3c1SinV8kFijFkpIkk1mocBGcaYfQAi8iYwxRjzODCp5meISCZQ7lqtrPm636p0wJL7IbYrjH/I7mqUUgpo5KktEYkQkYdEZL5rPUVEzvsF74YuQPU75TNdbXV5H7hSRJ4FVta2gYjMFpF0EUnPzs5uvkrttO55yN4BVz1pDYWilFJeoLFHJP/Euvy3atj4I8A7wBJPFNUQY0wxcHsD28wD5gGkpaWZlqjLo04dhBVPQOokSNXxMpVS3qOxne09jDFPYnVuV/0ib85LhY4AXautJ7raFIAxsPSXIAFw1f/ZXY1SSp2jsUFSLiLhWCP/IiI9gLJmrGM9kCIiySISAtwEfNiMn+/bdnwEez6Fsb+G2ES7q1FKqXM0Nkgewbq7vauILMa6SurBC/lCEXkDWAf0FpFMEbndNaXvHOBTrJsd3zbGbLuQz/c7ZQXwyYPQoR8Mv8vuapRS6jyN6iMxxnwmIhuA72Gd0vqZMSbnQr7QGDO9jvalwNIL+Uy/9uWfoOAYTHsVAu24WlspperX2Ku2vjDG5BpjPjbGLDHG5IhI67l3wy7HvoWvX4C0H0Fimt3VKKVUrRqaajcMiADiRaQtZzvYY6j/8lzlLmclfHQfRMTB+IftrkYpperU0LmSO4H7gM5Yl/9WBUk+1t3pylO2vAtHN8L/zIPwNnZXo5RSdWpo9N+ngadF5F6dyKoFVZTBl49Zk1X1u8HuapRSql6N7Wx/1jWBVV+s+Uiq2l+p+13qgm1YBHmHYNLfIKDFx9VUSqkmaVSQiMjDwBisIFkKXAWsptrAi6qZlBXAV09C0mjoMc7uapRSqkGN/e/uVGA8cNwY8yNgABDrsapas//+A4pzYMIjOs+IUsonNDZISowxTqBCRGKALM4d0kQ1h6IcWPOMNZ6WXu6rlPIRjb3DLV1E2mDNkrgBKMS6O101p1V/AUcRjP+d3ZUopVSjNRgkIiLA48aYPOAFEVkGxBhjvvN4da1J3mFrwqqBN0P73nZXo5RSjdaYOduNiCwF+rnWD3i6qFZpxeOAwPcbmmVYKaW8S2P7SDaKyFCPVtKaZe2Ab9+AYT+GNtr1pJTyLY3tIxkO3CIiB4EirDvcjTGmv8cqa02WPwYhUTDq53ZXopRSTdbYILnSo1W0ZpkbYOcSGPtbiIyzuxqllGqyxt7ZftDThbRaa/4KYW3ge3fbXYlSSl0QHX/DTrl7YccSGHo7hEbZXY1SSl0QDRI7/fcfEBgMw2bbXYlSSl0wrw8SEblIRBaIyLv1tfmc4pOw6TXodyNEd7S7GqWUumAeDRIRWSgiWSKytUb7RBHZJSIZIlLvjRPGmH3GmNsbavM56xdARQlcOsfuSpRSyi2engR8EdYEWGdGCRaRQOB54HIgE1gvIh8CgcDjNd4/yxiT5eEaW56jFL6ZBz0nQEIfu6tRSim3eDRIjDErRSSpRvMwIMMYsw9ARN4EphhjHgcmebIer7HlbSjKgkvvtbsSpZRymx19JF2Aw9XWM6ln/ncRiRORF4BBIjK3rrZa3jdbRNJFJD07O7sZy3eT0wlrn4OO/SD5+3ZXo5RSbvP0qS23GWNygbsaaqvlffOAeQBpaWnGYwU2VcZ/IGeXNRe7zjeilPIDdhyRHOHcuUwSXW2tw9pnILozXPIDuytRSqlmYUeQrAdSRCRZREKAm4APbaij5R3dDAdWWXexBwbbXY1SSjULT1/++wbWBFi9RSRTRG43xlQAc4BPgR3A28aYbZ6sw2usew5ComHITLsrUUqpZuPpq7am19G+FFjqye/2OnmHYev71tFImE53r5TyH15/Z7vfWD/fehxe7zUCSinlczRIWkJlBWx+A3pN1ImrlFJ+R4OkJWT8x7oBcdAtdleilFLNToOkJWx+DSLiIeUKuytRSqlmp0HiaUW5sGsZ9J+ml/wqpfySBomnbXkbnA49raWU8lsaJJ62eTF0GggdLra7EqWU8ggNEk869h0c3wKDfmh3JUop5TEaJJ60eTEEhsAl19tdiVJKeYwGiadUlMN3b0PqNRDRzu5qlFLKYzRIPGX3J1ByEgZqJ7tSyr9pkHjK5tchuhP0GGd3JUop5VEaJJ5QcAL2fA4DboKAQLurUUopj9Ig8YTv3gRTCQP1ai2llP/TIGluxsCmxdB1OMT3tLsapZTyOA2S5nZkozUn+8Cb7a5EKaVahAZJc9v8GgSFw8U6J7tSqnXw6AyJzUFELgJ+A8QaY6a62gKAPwAxQLox5mUbSzzLWQnbP4TUqyEsxu5qlFKqRXh6zvaFIpIlIltrtE8UkV0ikiEiv6rvM4wx+4wxt9dongIkAg4gs3mrdsPhb6A4B1In2V2JUkq1GE+f2loETKzeICKBwPPAVUBfYLqI9BWRfiKypMaSUMfn9gbWGmN+DtztwfqbZucSa0iUnhPsrkQppVqMR09tGWNWikhSjeZhQIYxZh+AiLwJTDHGPA409r/ymUC563llM5TqPmNg58eQfJme1lJKtSp2dLZ3AQ5XW890tdVKROJE5AVgkIjMdTW/D1wpIs8CK+t432wRSReR9Ozs7GYqvR7ZO+HUfmtsLaWUakW8vrPdGJML3FWjrRio2W9S833zgHkAaWlpxmMFVtm5xHrsdZXHv0oppbyJHUckR4Cu1dYTXW2+bedS6JIGMZ3srkQppVqUHUGyHkgRkWQRCQFuAj60oY7mk38Ujm7U01pKqVbJ05f/vgGsA3qLSKaI3G6MqQDmAJ8CO4C3jTHbPFmHx+1aaj1qkCilWiFPX7U1vY72pcBST353i9r5McT1hPhedleilFItTodIcVfpadi/CnpfDSJ2V6OUUi1Og8Rdez4Hp0PvZldKtVoaJO7a+TFEJkBimt2VKKWULTRI3FFRZh2R9J6oMyEqpVotDRJ3HFgF5QV6Wksp1appkLhj58cQHAnJ37e7EqWUso0GyYVyOmHXJ9BzPASH2V2NUkrZRoPkQh3dBAXH9LSWUqrV0yC5UDuXgARCyuV2V6KUUrbSILlQu5ZC0kiIaGd3JUopZSsNkgtx+og1/0jKlXZXopRSttMguRAH11iPyaPtrUMppbyABsmFOLAKwmKhwyV2V6KUUrbTILkQB1ZD95F6N7tSSqFB0nSnj8DJfZA0yu5KlFLKK2iQNFVV/4gGiVJKARokTaf9I0opdQ6vDxIRuU5E5ovIWyJyhavtIhFZICLvtnhB2j+ilFLn8PSc7QtFJEtEttZonygiu0QkQ0R+Vd9nGGP+ZYz5MXAXMM3Vts8Yc7vnKq+D9o8opdR5PDpnO7AIeA54papBRAKB54HLgUxgvYh8CAQCj9d4/yxjTJbr+W9d77OP9o8opdR5PBokxpiVIpJUo3kYkGGM2QcgIm8CU4wxjwPnjYAoIgI8AXxijNnoyXobpP0jSil1Hjv6SLoAh6utZ7ra6nIvMAGYKiJ3AYhInIi8AAwSkbm1vUlEZotIuoikZ2dnN0/l2j+ilFLn8fSpLbcZY54BnqnRlovVZ1Lf++YB8wDS0tKM24VU9Y8MvcPtj1JKKX9ixxHJEaBrtfVEV5t30/4RpZSqlR1Bsh5IEZFkEQkBbgI+tKGOptH+EaWUqpWnL/99A1gH9BaRTBG53RhTAcwBPgV2AG8bY7Z5so5mof0jSilVK09ftTW9jvalwFJPfnez0v4RpZSqk9ff2e4VtH9EKaXqpEHSGNo/opRSddIgaQztH1FKqTppkDREx9dSSql6aZA0RPtHlFKqXhokDdH+EaWUqpcGSUO0f0QppeqlQVIf7R9RSqkGaZDUx1ECfafARWPsrkQppbyW14/+a6v4nnDjKw1vp5RSrZgekSillHKLBolSSim3aJAopZRyiwaJUkopt2iQKKWUcosGiVJKKbdokCillHKLBolSSim3iDHG7ho8TkSygYNufEQ8kNNM5djJX/YDdF+8kb/sB+i+VOlujGnf0EatIkjcJSLpxpg0u+twl7/sB+i+eCN/2Q/QfWkqPbWllFLKLRokSiml3KJB0jjz7C6gmfjLfoDuizfyl/0A3Zcm0T4SpZRSbtEjEqWUUm5p9UEiIgtFJEtEtlZraycin4vIHtdjW1e7iMgzIpIhIt+JyGD7Kj+fiHQVkS9FZLuIbBORn7nafWp/RCRMRL4RkW9d+/Goqz1ZRL521fuWiIS42kNd6xmu15PsrL82IhIoIptEZIlr3Sf3RUQOiMgWEdksIumuNp/6+aoiIm1E5F0R2SkiO0RkhK/ti4j0dv1dVC35InJfS+9Hqw8SYBEwsUbbr4AvjDEpwBeudYCrgBTXMhv4RwvV2FgVwP8aY/oC3wPuEZG++N7+lAHjjDEDgIHARBH5HvB/wF+NMT2BU8Dtru1vB0652v/q2s7b/AzYUW3dl/dlrDFmYLVLSn3t56vK08AyY0wqMADr78en9sUYs8v1dzEQGAIUAx/Q0vthjGn1C5AEbK22vgvo5HreCdjlev4iML227bxxAf4NXO7L+wNEABuB4Vg3VQW52kcAn7qefwqMcD0Pcm0ndtdebR8SXf+YxwFLAPHhfTkAxNdo87mfLyAW2F/zz9YX96VaTVcAa+zYDz0iqV0HY8wx1/PjQAfX8y7A4WrbZbravI7rlMgg4Gt8cH9cp4I2A1nA58BeIM8YU+HapHqtZ/bD9fppIK5lK67X34AHAKdrPQ7f3RcDfCYiG0RktqvN536+gGQgG/in65TjSyISiW/uS5WbgDdcz1t0PzRIGmCs2PapS9tEJAp4D7jPGJNf/TVf2R9jTKWxDtcTgWFAqs0lXRARmQRkGWM22F1LMxlljBmMdYrkHhG5rPqLvvLzhXW0Nxj4hzFmEFDE2dM/gE/tC64+tsnAOzVfa4n90CCp3QkR6QTgesxytR8BulbbLtHV5jVEJBgrRBYbY953Nfvs/hhj8oAvsU7/tBGRINdL1Ws9sx+u12OB3BYutS4jgckicgB4E+v01tP45r5gjDnieszCOhc/DN/8+coEMo0xX7vW38UKFl/cF7CCfaMx5oRrvUX3Q4Okdh8CM13PZ2L1NVS1z3Bd+fA94HS1w0fbiYgAC4Adxpi/VHvJp/ZHRNqLSBvX83Csfp4dWIEy1bVZzf2o2r+pwHLX/8JsZ4yZa4xJNMYkYZ16WG6MuQUf3BcRiRSR6KrnWOfkt+JjP18AxpjjwGER6e1qGg9sxwf3xWU6Z09rQUvvh90dRHYvrj/8Y4AD638pt2Odk/4C2AP8B2jn2laA57HO128B0uyuv8a+jMI6hP0O2Oxarva1/QH6A5tc+7EV+J2r/SLgGyAD6xA+1NUe5lrPcL1+kd37UMd+jQGW+Oq+uGr+1rVsA37javepn69q+zMQSHf9nP0LaOuL+wJEYh21xlZra9H90DvblVJKuUVPbSmllHKLBolSSim3aJAopZRyiwaJUkopt2iQKKWUcosGiVJKKbdokCjVzEQkrtqw3sdF5Ei19ZBq24mILBeRGNd6YbXXrhaR3SLSXUTmiMgsO/ZFqcbQ+0iU8iAReQQoNMY8Vctr1wATjDH3u9YLjTFRIjIea5TWK40xe0UkAmtU10EtWbtSjaVHJErZ5xbODl0BgGsQxPnAJGPMXgBjTDFwQESGtXyJSjVMg0Qp+4wEqo8KHIo1VMd1xpidNbZNB0a3VGFKNYUGiVL2aWeMKai27gDWcna2xOqygM4tUpVSTaRBopR9KkSk+r9BJ3AjMExEfl1j2zCgpMUqU6oJNEiUss8urBF1z3D1h1wD3CIi1Y9MemGNhKyU19EgUco+H2MNLX8OY8xJYCLwWxGZ7GoeiTXlsFJeRy//VcomrpnrXjHGXN7AdoOAnxtjbm2ZypRqGj0iUcomxpqZbn7VDYn1iAceaoGSlLogekSilFLKLXpEopRSyi0aJEoppdyiQaKUUsotGiRKKaXcokGilFLKLf8f5diUDPm/k3IAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "%matplotlib inline\n",
    "import numpy as np\n",
    "from ase.vibrations import Vibrations\n",
    "from ase.io import read\n",
    "from ase.calculators.emt import EMT\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Calculated barriers\n",
    "barriers = {'01': 0.289, '10': 0.832}  # student: barriers = {'01': ?, '10': ?}\n",
    "\n",
    "directions = ['01', '10']\n",
    "\n",
    "def rate(T, ens0, ens1, Eb):\n",
    "    '''\n",
    "    T: Temperature\n",
    "    ens0: energies of initial state in a numpy array\n",
    "    ens1: energies of transition state in a numpy array\n",
    "    Eb: Transition state barrier\n",
    "    '''\n",
    "      \n",
    "    k = 8.617e-5   # Boltzmann constant in [eV/K]\n",
    "    h = 4.135e-15  # Planck constant in [eV*s]\n",
    "    \n",
    "    # Calculate partition functions\n",
    "    z0 = np.prod(np.exp(-ens0/(2*k*T)) / (1 - np.exp(-ens0/(k*T))))  # student: z0 = ?\n",
    "    z1 = np.prod(np.exp(-ens1/(2*k*T)) / (1 - np.exp(-ens1/(k*T))))  # student: z1 = ?\n",
    "\n",
    "    # Calculate rate\n",
    "    r = k*T/h * z1/z0 * np.exp(-Eb/(k*T))  # student: r = ?\n",
    "    return r\n",
    "\n",
    "\n",
    "energies = {}\n",
    "for direction in directions:\n",
    "    energies[direction] = []\n",
    "    for ii, index in enumerate([-10, -6]):  # Loop through initial and transition state\n",
    "        # Use loop index as:  ii = 0: Initial, ii = 1: Final\n",
    "        \n",
    "        slab = read('a{}-CINEB.traj'.format(direction), index=index)\n",
    "\n",
    "        slab.set_calculator(EMT())\n",
    "        \n",
    "        vib = Vibrations(slab, \n",
    "                         name='{}_{}-vib'.format(ii, direction),\n",
    "                         indices=[112],  # Atom index to vibrate\n",
    "                         nfree=4, delta=0.01)\n",
    "        vib.run()\n",
    "        en = np.real(vib.get_energies()) + 1e-8 # Get real part of vibrational energies\n",
    "        energies[direction].append(en)\n",
    "        \n",
    "plt.figure()\n",
    "labels = {'01': 'Short Bridge',\n",
    "          '10': 'Long Bridge'}\n",
    "\n",
    "x = np.linspace(100, 700, 50)  # Temperature range in K\n",
    "for direction in directions:\n",
    "    ens0, ens1 = energies[direction]\n",
    "    Eb = barriers[direction]\n",
    "    y = [rate(T, ens0, ens1, Eb) for T in x]\n",
    "    plt.semilogy(x, y, label=labels[direction])\n",
    "plt.legend(loc='best')\n",
    "plt.xlabel('T (K)')\n",
    "plt.ylabel('rate (1/s)')\n",
    "plt.savefig('rates.png')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
