import stuff
from stuff import f, foo

stuff.foo()
print(stuff.f(5))

foo()
print(f(5, n=8))
